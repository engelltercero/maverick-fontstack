## Maverick fontstack

#### The maverick fontstack is the default font compilation avaible on the AM/L Rollingstone OS, contains only fonts licenced under the OFL.

##### Sample of fonts provided by the stack

- ##### Inter

- ##### Inter Hewn

- ##### January

- ##### Karasuma Gothic

- ##### Noto Fonts

- ##### RedHat / IBM Plex

- ##### Libertus

- ##### Cascadia Code / Mono

- ##### Public Sans

- ##### QtChancery / QtBookmann

- ##### Academico

- ##### Roboto Fonts

- ##### Newsreader



#### This fontstack includes various type of fonts, from otf to woff2 webfonts, enjoy the fonts.



### To Do List

- [x] ##### Add lightweight Noto fonts

- [ ] ##### Add Adobe opensource fonts

- [x] ##### Add Microsoft opensource fonts

- [ ] ##### Add a font manager script